import "./App.css";
import { List } from "./components/list/List";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>HN Feed</h1>
        <h3>{"We <3 hacker news!"}</h3>
      </header>
      <body>
        <List></List>
      </body>
    </div>
  );
}

export default App;
