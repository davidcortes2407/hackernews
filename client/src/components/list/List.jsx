import React, { useState, useEffect } from "react";
import "./List.scss";

export const List = () => {
  const API = "http://localhost:4011/hackernews";
  const [data, setData] = useState();

  const getInfo = () => {
    return fetch(API)
      .then((res) => res.json())
      .then((res) => setData(res));
  };

  useEffect(() => {
    getInfo();
  }, []);

  const handler = (link) => {
    link && window.open(link, "_blank");
  };

  const deleteHandler = (e, element) => {
    e.preventDefault();
    element.isdeleted = true;
    const requestOptions = {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ ...element }),
    };

    fetch(`${API}/${element._id}`, requestOptions)
      .then((res) => res.json())
      .then(() => setData(data.filter((elem) => elem._id !== element._id)));
  };

  return (
    <div className="list">
      {!data
        ? "loading"
        : data
            .sort(
              (objA, objB) =>
                new Date(objA.createdAt) - new Date(objB.createdAt)
            )
            .filter((element) => !element.isdeleted)
            .filter((element) => element.title || element.story_title)
            .map((element) => {
              return (
                <div className="list--element" key={element._id}>
                  <div
                    className="list--title"
                    onClick={() => handler(element.url || element.story_url)}
                  >
                    <span className="list--title__black">
                      {" "}
                      {element.story_title
                        ? `${element.story_title} `
                        : `${element.title} `}
                    </span>
                    <span className="list--title__gray">{`- ${element.author} -`}</span>
                  </div>
                  <div className="list--date">
                    {new Date(element.createdAt).getDay() ===
                    new Date().getDay()
                      ? new Date(element.createdAt).toLocaleString("en-US", {
                          hour: "numeric",
                          minute: "numeric",
                          hour12: true,
                        })
                      : new Date().getDay() -
                          new Date(element.createdAt).getDay() ===
                        1
                      ? "Yesterday"
                      : new Date(element.createdAt).toLocaleString("en-US", {
                        month: "short",
                        day: "numeric",
                      })}
                  </div>
                  <div
                    className="list--button"
                    onClick={(e) => deleteHandler(e, element)}
                  >
                    <i className="fa">&#xf014;</i>
                  </div>
                </div>
              );
            })}
    </div>
  );
};
