import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HackernewsModule } from './hackernews/hacker.module';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [
    HackernewsModule,
    MongooseModule.forRoot('mongodb://mongo_db:27017/hackernews'),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
