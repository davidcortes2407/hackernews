export interface HackerNew {
  id?: string;
  story_title?: string;
  title?: string;
  author?: string;
  created_at?: string;
  isdeleted?: boolean;
  url?: string;
  story_url?: string;
}
