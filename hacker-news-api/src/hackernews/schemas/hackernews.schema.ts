import * as mongoose from 'mongoose';

export const HackernewSchema = new mongoose.Schema(
  {
    story_title: String,
    title: String,
    author: String,
    isdeleted: Boolean,
    url: String,
    story_url: String,
  },
  { timestamps: true },
);
