export class HAckernewDTO {
  readonly title: string;
  readonly story_title: string;
  readonly author: string;
  readonly url: string;
  readonly story_url: string;
  readonly isdeleted: boolean;
}
