import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Param,
  Body,
} from '@nestjs/common';
import { HackernewsService } from './hackernews.service';
import { HAckernewDTO } from './dtos/hackernew.dto';
import { HackerNew } from './interfaces/hackernews.interface';

@Controller('hackernews')
export class HackernewsController {
  constructor(private readonly hackernewsService: HackernewsService) {}
  @Get()
  find(): Promise<HackerNew[]> {
    return this.hackernewsService.findAll();
  }

  @Get('uploadinfo')
  uploadInfo(): void {
    return this.hackernewsService.uploadInfo();
  }

  @Post()
  create(@Body() hackernew: HAckernewDTO): Promise<HackerNew> {
    return this.hackernewsService.create(hackernew);
  }

  @Put(':id')
  update(@Param('id') id, @Body() hackernew: HAckernewDTO): Promise<HackerNew> {
    return this.hackernewsService.update(id, hackernew);
  }

  @Delete(':id')
  delete(@Param('id') id): Promise<HackerNew> {
    return this.hackernewsService.delete(id);
  }
}
