import { Injectable } from '@nestjs/common';
import { HackerNew } from './interfaces/hackernews.interface';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { HttpService } from '@nestjs/axios';
import { map } from 'rxjs/operators';

@Injectable()
export class HackernewsService {
  constructor(
    @InjectModel('Hackernew') private readonly hackernewModel: Model<HackerNew>,
    private httpService: HttpService,
  ) {}

  async find(id: string): Promise<HackerNew> {
    return await this.hackernewModel.findOne({ _id: id });
  }

  async findAll(): Promise<HackerNew[]> {
    return await this.hackernewModel.find();
  }

  async create(hackernew: HackerNew): Promise<HackerNew> {
    const newHackernew = new this.hackernewModel(hackernew);
    newHackernew.isdeleted = false;
    return await newHackernew.save();
  }

  async update(id: string, hackernew: HackerNew): Promise<HackerNew> {
    return await this.hackernewModel.findByIdAndUpdate(id, hackernew, {
      new: true,
    });
  }

  async delete(id: string): Promise<HackerNew> {
    return await this.hackernewModel.findByIdAndRemove(id);
  }

  uploadInfo(): void {
    const hackernews = this.httpService
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .pipe(
        map((response) => response.data),
        map((data) => data.hits),
        map((hits) => {
          return hits.map((elem) => {
            return {
              story_title: elem.story_title,
              title: elem.title,
              author: elem.author,
              created_at: elem.created_at,
              url: elem.url,
              story_url: elem.story_url,
              isdeleted: false,
            };
          });
        }),
      );

    hackernews.subscribe((elements) => {
      elements.map(async (hackernew) => {
        const newHackernew = new this.hackernewModel(hackernew);
        await newHackernew.save();
      });
    });
  }
}
