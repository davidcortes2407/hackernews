import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HackernewsController } from './hackernews.controller';
import { HackernewsService } from './hackernews.service';
import { HackernewSchema } from './schemas/hackernews.schema';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Hackernew', schema: HackernewSchema }]),
    HttpModule,
  ],
  controllers: [HackernewsController],
  providers: [HackernewsService],
})
export class HackernewsModule {}
